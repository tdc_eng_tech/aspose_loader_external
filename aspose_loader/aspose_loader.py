__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'

import jpype
import jpype.imports
from jpype.types import *
from typing import Literal
from pathlib import Path
import os
import zipfile
import tempfile

MODULENAME = Literal['cells', 'words', 'diagram', 'imaging', 'slides']

if not os.environ.get('JAVA_HOME'):
    os.environ['JAVA_HOME'] = r'c:\Program Files (x86)\WordFast_AP_GUI\Java\jdk-11'

#def apply_license(lic_str: str) -> License:
#    """Applies the Aspose license to the Document
#
#    Returns:
#        License: The value of the license application
#    """
#    #license = jpype.JClass('com.aspose.words.License')
#    license = License()
#    lic_str = r'lic\aspose.license'
#    license.setLicense(lic_str)
#    return license

class OfficeJar:
    """
    Read a Zip file
    """
    def __init__(self, ap_dir= r'c:\Program Files (x86)\Analysis Package\AP4\lib'):
        """
        """
        self._ap_dir = ap_dir
        self._office_file = self.__find_office()
        self._tmpdir = None
    
    @property
    def ap_dir(self):
        return self._ap_dir
    
    def __find_office(self):
        ap_lib_pth = Path(self._ap_dir)
        for file in ap_lib_pth.glob('*.jar'):
            if 'msoffice' in file.stem:
                return str(file)
    
    def get_license(self):
        self._tmpdir = tempfile.mkdtemp()
        zip_office = zipfile.ZipFile(self._office_file)
        return zip_office.extract(zip_office.getinfo('aspose.license'), self._tmpdir)
        
    

class AsposeLoader:
    """
    """
    def __init__(self, module: MODULENAME,  *modules,  aspose_dir: str=r'c:\Program Files (x86)\Analysis Package\AP4\lib',):
        """
        Initialize the module: Words, Cells, Slides
        """
        self._module_name = module
        self._aspose_dir = aspose_dir
        self._module_names = [module for module in modules]
        self._jar = self.__find_module()
        self._license = self.__get_license()
        self.__jpype_loader()
        
    
    @property
    def module_name(self):
        return self._module_name
    
    @property
    def aspose_dir(self):
        return self._aspose_dir
    
    @property
    def license(self):
        return self._license
    
    def __find_module(self):
        modules = list()
        if self.module_name:
            aspose_pth = Path(self.aspose_dir)
            for file in aspose_pth.glob('*.jar'):
                if 'com.aspose' in file.stem:
                    if self.module_name in file.stem:
                        modules.append(str(file))
                    for module in self._module_names:
                        if module in file.stem:
                            modules.append(str(file))
        if modules:
            return modules
        else:
            raise Exception('There were not modules found. Something went wrong')
    
    def __jpype_loader(self):
        """
        """
        try:
            jpype.startJVM(classpath=self._jar)
        except SystemError as err:
            print('Error:', err)
    
    def __get_license(self):
        return OfficeJar(self.aspose_dir).get_license()

if __name__ == '__main__':
    asp = AsposeLoader('cells', 'words', 'diagram')
    print(asp.license)
    print(asp._jar)
    from com.aspose.cells import License
    License().setLicense(asp.license)
