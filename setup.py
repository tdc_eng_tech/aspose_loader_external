__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2021. All rights are reserved.'
__license__ = 'GPL 3'

from setuptools import setup

with open('README.md', 'r', encoding='utf-8') as fh:
    long_description = fh.read()

setup(name='aspose_loader', 

        version='0.1.1', 

        description='Library to load Aspose modules.', 
        long_description=long_description, 
        classifiers=[
            'Development Status :: 3 - Alpha', 
            'License :: OSI Approved :: LGPL License 3', 

            'Programming Language :: Python :: 3.8.6', 

            'Topic :: Text Processing :: Translation'
        ], 
        keywords='aspose office word excel', 
        author='Llorenç Suau', 
        author_email='lsuau@translations.com', 
        license='LGPL 3', 
        packages=['aspose_loader'],
        install_requires=['JPype1'], 
        zip_safe=False, 
        python_requires='>=3.7'
        )
